FROM node:14.18.1 as builder
MAINTAINER zac "zacgrli@tencent.com"

# ENV NODE_ENV production

RUN echo " ------------------Web打包 --------------------"

WORKDIR /chainmaker-explorer-web

COPY . /chainmaker-explorer-web

RUN npm install -g npm@7.24.2
RUN npm install
RUN npm rebuild node-sass
RUN npm run build

RUN echo " ------------------Web容器部署启动 --------------------"

FROM nginx:1.19.2
COPY --from=builder /chainmaker-explorer-web/build /usr/share/nginx/html
COPY deploy/nginx/conf.d /etc/nginx/conf.d
EXPOSE 8080
