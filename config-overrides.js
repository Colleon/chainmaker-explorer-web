/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

/* eslint-disable @typescript-eslint/no-require-imports */
const { override, addWebpackAlias, addBabelPlugin } = require('customize-cra');
const path = require('path');
const resolvePath = (src) => path.resolve(__dirname, src);
module.exports = override(
  addWebpackAlias({
    '@src': resolvePath('src'),
    '@imgs': resolvePath('src/assets/imgs'),
    '@svgs': resolvePath('src/assets/svgs'),
    '@components': resolvePath('src/components'),
  })
);
