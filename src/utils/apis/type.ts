/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */
import { AxiosResponse } from 'axios';

export interface ResponseData<T> {
  Data: T;
}
export interface ResponseList<T> {
  TotalCount: number;
  GroupList: T[];
}

export interface ResponseError {
  Error: {
    Code: string;
    Message: string;
  };
}
export interface ResponseIf<T> {
  Response: T;
}
export type ResponseInfo<T> = AxiosResponse<ResponseIf<ResponseData<T> | ResponseList<T> | ResponseError>>;
export type Fetch<P, T> = (params: P) => Promise<T>;
