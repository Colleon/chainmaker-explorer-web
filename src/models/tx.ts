/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */
import { PagingWithChainId } from './common';
export interface GetTxListParam extends PagingWithChainId {
  /**
   * 交易id值
   */
  TxId?: string;
  /**
   * 合约名
   */
  ContractName?: string;
  /**
   * 区块hash值
   */
  BlockHash?: string;
  /**
   * 开始时间
   */
  StartTime?: number;
  /**
   * 结束时间
   */
  EndTime?: number;
  /**
   * 用户名
   */
  Sender?: string;
  /**
   * 用户地址
   */
  UserAddr?: string;
}

export interface Tx {
  /**
   * 区块逻辑ID
   */
  Id: number;
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 交易Id
   */
  TxId: string;
  /**
   * 交易发送者
   */
  Sender: string;
  /**
   * 交易发送者地址
   */
  UserAddr: string;
  /**
   * 交易发送组织
   */
  SenderOrg: string;
  /**
   * 合约名
   */
  ContractName: string;
  /**
   * 交易状态
   */
  Status: string;
  /**
   * 区块hash
   */
  BlockHash: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}

export interface TxInfo {
  /**
   * 交易Id
   */
  TxId: string;
  /**
   * 交易hash
   */
  TxHash: string;
  /**
   * 详情内字段是否可见，1为不可见
   */
  ShowStatus: 0 | 1;
  /**
   * 交易类型
   */
  TxType: string;
  /**
   * 区块hash
   */
  BlockHash: string;
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 交易发送者
   */
  Sender: string;
  /**
   * 交易发送所属组织
   */
  OrgId: string;
  /**
   * 合约名
   */
  ContractName: string;
  /**
   * 合约版本
   */
  ContractVersion: string;
  /**
   * 交易状态
   */
  TxStatusCode: string;
  /**
   * 交易merkle哈希
   */
  TxRootHash: string;
  /**
   * 合约执行结果码
   */
  ContractResultCode: number;
  /**
   * 合约执行结果
   */
  ContractResult: string;
  /**
   * 交易信息
   */
  RwSetHash: string;
  /**
   * 合约调用方法
   */
  ContractMethod: string;
  /**
   * 参数
   */
  ContractParameters: string;
  /**
   * 参数列表
   */
  ContractParametersList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
    /**
     * 解码的值
     */
    decodeValue?:{
      index: number;
      type:string;
      value:string;
    }[];
  }[];
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 交易发送者地址
   */
  UserAddr: string;
  /**
   * 合约信息
   */
  ContractMessage?: string;
  /**
   * GAS消耗量
   */
  GasUsed?: string;
  /**
   * 读集
   */
  ContractRead: string;
  /**
   * 写集
   */
  ContractWrite: string;
  /**
   * 读集列表
   */
  ContractReadList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
  }[];
  /**
   * 写集列表
   */
  ContractWriteList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
  }[];
  /**
   * 代付者
   */
  Payer: string;
  Event: string;
  EventList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
  }[];
  /** 合约类型 */
  RuntimeType: string;
}
