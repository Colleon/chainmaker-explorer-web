/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

import { PagingWithChainId } from '.';

export interface GetUserListParam extends PagingWithChainId {
  /**
   * 用户id
   */
  UserId: string;
  /**
   * 组织id
   */
  OrgId?: string;
}

export interface UserItem {
  /**
   * 交易逻辑id
   */
  Id: number;
  /**
   * 用户id
   */
  UserId: string;
  /**
   * 用户地址
   */
  UserAddr: string;
  /**
   * 用户角色
   */
  Role: string;
  /**
   * 组织id
   */
  OrgId: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}
