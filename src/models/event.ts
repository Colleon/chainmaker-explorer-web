/*
 *
 *  *
 *  *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  *  SPDX-License-Identifier: Apache-2.0
 *  *
 *
 */

import { PagingWithChainId } from './common';

export interface GetEventListParam extends PagingWithChainId {
  /**
   * 合约名
   */
  ContractName?: string;
}

export interface EventItem {
  /**
   * 事件主题
   */
  Topic: string;
  /**
   * 事件信息
   */
  EventInfo: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}
